set number relativenumber
set hlsearch
set incsearch
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set fileformat=unix
set encoding=utf-8
set list!

" Make nerdcommenter's keybinding a little easier
" Obviously you will need Nerd Commenter for this
" https://github.com/preservim/nerdcommenter
map gc <Plug>NERDCommenterToggle

map <tab> I<tab><esc>:s/\s\+$//e<enter>j
