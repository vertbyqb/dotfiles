;; Emacs Packages

(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org"   . "https://orgmode.org/elpa/")
			 ("elpa"  . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package ivy
  :diminish
  :config
  (ivy-mode 1))

(use-package counsel
  :bind (("M-x"     . counsel-M-x)
	 ("C-x b"   . counsel-ibuffer)
	 ("C-x C-f" . counsel-find-file)
	 :map minibuffer-local-map
	 ("C-r"     . counsel-minibuffer-history)))

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

(use-package doom-themes
  :init
  (load-theme 'doom-city-lights))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command]  . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key]      . helpful-key))

(use-package undo-tree
  :config
  (global-undo-tree-mode))

;; Evil mode :)
(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-undo-system 'undo-tree)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package evil-numbers)

;; A function to toggle show-trailing-whitespace
(defun show-trailing-whitespace-in-buffer ()
  (interactive)
  (if (eq show-trailing-whitespace nil)
      (progn
	(setq show-trailing-whitespace t)
	(message "show-trailing-whitespace on"))
    (progn
      (setq show-trailing-whitespace nil)
      (message "show-trailing-whitespace off"))))

(use-package general
  :config
  (general-create-definer my-leader-def
			  :keymaps '(normal insert visual emacs)
			  :prefix "SPC"
			  :global-prefix "C-SPC")

  (my-leader-def
    "p"   '(recentf-mode                       :which-key "toggle recent file mode")
    "d"   '(delete-trailing-whitespace         :which-key "delete trailing whitespace")
    "s"   '(show-trailing-whitespace-in-buffer :which-key "toggle showing trailing whitespace")
    "u"   '(elfeed-update                      :which-key "update feeds")
    "C-a" '(evil-numbers/inc-at-pt             :which-key "increment number")
    "C-x" '(evil-numbers/dec-at-pt             :which-key "decrement number")
    "m"   '(mu4e                               :which-key "email")
    ))

(use-package evil-nerd-commenter)

(use-package magit)

(use-package vterm)

(use-package fic-mode
  :hook (prog-mode . fic-mode))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package elfeed)

;; ement.el
;; (package-install 'quelpa-use-package)
;; (require 'quelpa-use-package)

;; ;; Install `plz' HTTP library (not on MELPA yet).
;; (use-package plz
;;   :quelpa (plz :fetcher github :repo "alphapapa/plz.el"))

;; ;; Install Ement.
;; (use-package ement
;;   :quelpa (ement :fetcher github :repo "alphapapa/ement.el"))

(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

(use-package mu4e
  :ensure nil
  ;; :load-path "/usr/share/emacs/site-lisp/mu4e/"
  ;; :defer 20 ; Wait until 20 seconds after startup
  :config

  ;; Configure the function to use for sending mail
  (setq message-send-mail-function 'smtpmail-send-it)

  ;; This is set to 't' to avoid mail syncing issues when using mbsync
  (setq mu4e-change-filenames-when-moving t)

  ;; Refresh mail using isync every 10 minutes
  (setq mu4e-update-interval (* 10 60))
  (setq mu4e-get-mail-command "~/.local/bin/mbsync -a")
  (setq mu4e-maildir "~/.local/share/mail/")

  (setq mu4e-drafts-folder "/Onionmail/Drafts")
  (setq mu4e-sent-folder   "/Onionmail/Sent Mail")
  (setq mu4e-refile-folder "/Onionmail/Archive")
  (setq mu4e-trash-folder  "/Onionmail/Trash")

  ;; Configure SMTP
  (setq smtpmail-smtp-server "mail.onionmail.org"
        smtpmail-smtp-service 587
        smtpmail-stream-type  'starttls
	user-mail-address     "vertbyqb@onionmail.org"
	user-full-name        "Vertbyqb")

  ;; Make sure plain text mails flow correctly for recipients
  (setq mu4e-compose-format-flowed t)

  ;; Signature
  (setq mu4e-compose-signature
	(concat
	 "Vertbyqb\n"
	 "lbry://@vertbyqb#81bec0b66ff34a1378581751958f5b98f9043d17\n\n"
	 "My PGP public key's fingerprint is A69ABC24BA63FE34AFD2AB31D8C0BE31F88F2D57.\n"
         "It can be found at https://keys.openpgp.org/vks/v1/by-fingerprint/A69ABC24BA63FE34AFD2AB31D8C0BE31F88F2D57"))

  (setq mml-secure-openpgp-signers '("A69ABC24BA63FE34AFD2AB31D8C0BE31F88F2D57"))
  (add-hook 'message-send-hook 'mml-secure-message-sign-pgpmime)
  (setq mm-verify-option 'known)

  ;; HTML emails with org-mime
  (add-hook 'message-send-hook 'org-mime-confirm-when-no-multipart)
  (setq org-mime-export-options '(:section-numbers nil
                                  :with-author nil
                                  :with-toc nil))

  (setq mu4e-maildir-shortcuts
      '(("/Onionmail/INBOX"       . ?i)
        ("/Onionmail/Sent Mail"   . ?s)
        ("/Onionmail/Trash"       . ?t)
        ("/Onionmail/Drafts"      . ?d))))

(use-package org-mime
  :ensure t)

(defun dw/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (auto-fill-mode 0)
  (visual-line-mode 1)
  (setq evil-auto-indent nil))

(use-package org
  :hook (org-mode . dw/org-mode-setup)
  :config
  (setq org-ellipsis " ▾")
  (add-hook 'org-mode-hook 'org-indent-mode)
  (setq org-src-fontify-natively t
      org-src-tab-acts-natively t
      org-confirm-babel-evaluate nil
      org-edit-src-content-indentation 0))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(use-package org-tempo
  :ensure nil) ;; tell use-package not to try to install org-tempo since it's already there.

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/Documents/Code/projects")
    (setq projectile-project-search-path '("~/Documents/Code/projects")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
 :after projectile
 :config
 (counsel-projectile-mode 1))

;; Replace list hyphen with dot
(font-lock-add-keywords 'org-mode
                        '(("^ *\\([-]\\) "
                           (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

(use-package python-black
  :demand t
  :after python
  :hook (python-mode . python-black-on-save-mode-enable-dwim))

(use-package lua-mode)
