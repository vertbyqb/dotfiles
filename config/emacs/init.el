;; Stop custom cluttering up init.el
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)

;; Make things look nicer
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 10)
(menu-bar-mode -1)

(setq visible-bell t)

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Set Meta to Super
;;(setq x-super-keysym 'meta)

;; Get all the packages
(load (expand-file-name "packages.el" user-emacs-directory))

;; Line numbers
(column-number-mode)
(global-display-line-numbers-mode t)
(setq display-line-numbers-type 'relative)

;; Don't display line numbers in certain modes
(dolist (mode '(term-mode-hook
		shell-mode-hook
		org-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Put backups in my Emacs directory
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))

;; Recent files
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

;; Override the default Emacs commenter with Nerd Commenter
(global-set-key (kbd "M-;") 'evilnc-comment-or-uncomment-lines)

;; Also map Nerd Commenter to "gc" in normal state
(evil-global-set-key 'normal "gc" 'evilnc-comment-or-uncomment-lines)

;; Go to last buffer with M-o
(global-set-key (kbd "M-o")  'mode-line-other-buffer)

(server-start)

;; Use evil search instead of default search
(evil-select-search-module 'evil-search-module 'evil-search)

;; Use flyspell in all major modes
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'prog-mode-hook 'flyspell-prog-mode)

;; Configure my dashboard
(setq dashboard-startup-banner "~/Images/saintignucius.png") ;; Dashboard does not seem to support JPEG images
(setq dashboard-init-info "I bless your computer, my child!")
(setq dashboard-footer-messages
      '(
      "Happy Hacking!"
      "There is no system but GNU, and Linux is one of its kernels."
      "Emacs is great, once you turn it into Vim."
      "Join us now and share the software!"
      "A great operating system, lacking only a decent editor."
      "Richard Stallman is proud of you"

      ;; (GNU) EMACS Acronyms
      "Eight Megabytes And Constantly Swapping"
      "Eventually Munches All Computer Storage"
      "Eradication of Memory Accomplished with Complete Simplicity"
      "Generally Not Used Except by Middle Aged Computer Scientists"
      "Every Mode Accelerates Creation of Software"
      "Emacs Makes Any Computer Slow"
      "Powered by Lots of Infuriating and Silly Parentheses!"
      ))

(setq dashboard-items '((recents   . 4)
                        (bookmarks . 4)
                        (projects  . 4)))

(load (expand-file-name "elfeed.el" user-emacs-directory))
(global-set-key (kbd "C-x w") 'elfeed)

;; Font for emojis
(setf use-default-font-for-symbols nil)
(set-fontset-font t 'unicode "JoyPixels" nil 'append)

(set-face-attribute 'default nil
  :font "Iosevka Nerd Font Mono"
  :height 130)
(set-face-attribute 'fixed-pitch nil
  :font "Iosevka Fixed"
  :height 130)

;; Makes commented text and keywords italics.
;; This is working in emacsclient but not emacs.
;; Your font must have an italic face available.
(set-face-attribute 'font-lock-comment-face nil
  :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil
  :slant 'italic)

(dolist (face '((org-level-1 . 1.2)
                (org-level-2 . 1.1)
                (org-level-3 . 1.05)
                (org-level-4 . 1.0)
                (org-level-5 . 1.1)
                (org-level-6 . 1.1)
                (org-level-7 . 1.1)
                (org-level-8 . 1.1)))
  (set-face-attribute (car face) nil :font "Iosevka Etoile" :weight 'regular :height (cdr face)))

;; Take advantage of the number pad in normal mode for increment and decrement
(define-key evil-normal-state-map (kbd "<kp-add>") 'evil-numbers/inc-at-pt)
(define-key evil-normal-state-map (kbd "<kp-subtract>") 'evil-numbers/dec-at-pt)

;; Show whitespace in prog-mode buffers by default
(add-hook 'prog-mode-hook (lambda () (setq show-trailing-whitespace t)))

(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))

;; :q should kill the current buffer rather than quitting emacs entirely
(evil-ex-define-cmd "q" 'kill-this-buffer)

;; Make scripts executable automatically
(add-hook 'after-save-hook
  'executable-make-buffer-file-executable-if-script-p)
